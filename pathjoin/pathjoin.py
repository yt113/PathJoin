
# Define a concrete data prototype:

#		   		A0       		A1
# A = [	((x1,x2,x3,...), (x4,x5,x6,...   )),
#	 	((  		  ), (  			 )),
#		((   		  ), (				 ))]
#
#
# A = [((1, 10),(2)), ((2, 10), (3))]


# Define a abstract data prototype:
class AttributesPairs():
	def __init__(self, pairs):
		self.pairs = pairs

	# sort by the left attributes
	def sortByLeft(self):
		self.pairs.sort(key = lambda x: x[0])

	# sort by the right attributes
	def sortByRight(self):
		self.pairs.sort(key = lambda x: x[1])

	# return an iterator of attributes pairs
	def getPairIterator(self):
		for pair in self.pairs:
			yield pair

	# return a counting dictionary grouped by the left attributes 
	def countByLeft(self):
		from collections import Counter
		return dict(Counter(x[0] for x in self.pairs))

	# return a counting dictionary grouped by the right attributes 
	def countByRight(self):
		from collections import Counter
		return dict(Counter(x[1] for x in self.pairs))

class ComputePathJoin():
	def __init__(self, pairs_list):
		self.aps_list = [AttributesPairs(pairs) for pairs in pairs_list]

	def computePrelist(self):
		prelist = []
		for i, aps in enumerate(self.aps_list):
			if i == 0:
				prelist.append(aps.countByRight())
			else:
				from collections import defaultdict
				new_pre = defaultdict(int)
				prev_pre = prelist[-1]
				for ap in aps.getPairIterator():
					if ap[0] in prev_pre:
						new_pre[ap[1]] += prev_pre[ap[0]]
				prelist.append(dict(new_pre))
		self.prelist = prelist
		return self.prelist

	def computePostlist(self):
		postlist = []
		for i, aps in enumerate(self.aps_list[::-1]):
			if i == 0:
				postlist.append(aps.countByLeft())
			else:
				from collections import defaultdict
				new_post = defaultdict(int)
				prev_post = postlist[-1]
				for ap in aps.getPairIterator():
					if ap[1] in prev_post:
						new_post[ap[0]] += prev_post[ap[1]]
				postlist.append(dict(new_post))
		postlist.reverse()
		self.postlist = postlist
		return self.postlist

	def computeLocalSensitivity(self):
		ls = max(max(self.prelist[-2].values()), max(self.postlist[1].values()))
		for i in range(1, len(self.aps_list)-1):
			ls = max(ls, max(self.prelist[i-1].values()) * max(self.postlist[i+1].values()))
		self.ls = ls
		return ls

	def solution(self):
		self.computePrelist()
		self.computePostlist()
		self.computeLocalSensitivity()
		return self.ls

def test():
	test_aps_list = \
	[
		[
			(('Adim', '18'), ('male')),
			(('Hank', '20'), ('male')),
			(('Andrew', '30'), ('male')),
			(('Alice', '21'), ('female'))
		],
		[
			(('male'), ('Duke', 'General')),
			(('male'), ('James', 'Soldeir')),
			(('female'), ('Ava', 'Scientist'))
		],
		[
			(('Duke', 'General'), (10)),
			(('Duke', 'General'), (11)),
			(('Ava', 'Scientist'), (120)),
			(('Ava', 'Scientist'), (10))
		],
		[
			((10), (1)),
			((10), (2)),
			((120), (1))
		]
	]
	sol = ComputePathJoin(test_aps_list)
	print 'local sensitivity:', sol.solution()
	print 'prelist:', sol.prelist
	print 'postlist', sol.postlist
	print 'preMax:', [max(x.values()) for x in sol.prelist]
	print 'postMax:', [max(x.values()) for x in sol.postlist]
	
	'''
	Test Result:

	local sensitivity: 9
	prelist: [{'male': 3, 'female': 1}, {('Duke', 'General'): 3, ('James', 'Soldeir'): 3, ('Ava', 'Scientist'): 1}, {120: 1, 10: 4, 11: 3}, {1: 5, 2: 4}]
	postlist [{('Adim', '18'): 2, ('Hank', '20'): 2, ('Alice', '21'): 3, ('Andrew', '30'): 2}, {'male': 2, 'female': 3}, {('Duke', 'General'): 2, ('Ava', 'Scientist'): 3}, {120: 1, 10: 2}]
	preMax: [3, 3, 4, 5]
	postMax: [3, 3, 3, 2]
	'''


if __name__ == '__main__':
	test()
